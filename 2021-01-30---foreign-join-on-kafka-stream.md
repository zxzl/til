---
template: post
slug: /foreign-join-on-kafka-stream
title: "foreign join on kafka stream"
author: "Hyeungshik Jung"
date: 2021-01-30 16:26:33
category: "til"
draft: false
---

메시지의 키 = 파티션 키 가지고 스트림-스트림 조인하는 것은 많이 봤는데...요구사항이 항상 그런 것이 아니어서 키가 아닌 필드 가지고 조인해야할 경우도 있다. 이런거를 뭐라고 부르는지를 모르다가 이런 기능을 Foreign-Key Join이라고 부른다는 것을 알게 되었다.

https://www.confluent.io/resources/kafka-summit-2020/crossing-the-streams-the-new-streaming-foreign-key-join-feature-in-kafka-streams/

영상은 이메일, 이름 등을 입력해야 볼 수 있지만, 슬라이드만 봐도 큰 무리는 없다. 카프카 2.4부터 있다니까 지금 쓸 수 있는 기능이기도 하다 (KSQL에서는 아직 지원하지 않는다 https://github.com/confluentinc/ksql/issues/4424)

좀 우려?가 됐던 것은 비용이다. Ktable - Ktable을 join하면 예를 들어서 left join이라고 하면 왼쪽 테이블 크기 만큼 rocksdb 스토리지가 필요하고, 카프카 브로커 스토리지도 필요하다 (Log compaction이 되도 어쨌든 키의 갯수에 비례해서 공간이 필요하다 [sizing 문서](https://docs.confluent.io/platform/current/streams/sizing.html) 참조). 게다가 당연히 기존에 있던 Ktable - ktable들도 공간을 차지한다. 원래부터 카프카를 confluent에서 밀듯이 source of truth로 삼고, 스트림을 많이 썼으면 별 상관이 없겠지만, 단순히 메시지 전송 레이어로만 생각을 했다면 투자를 훨씬 늘려야 할 것이다. 사실 Stateful 스트리밍이 용량 차지하는 것은 당연한 것이고, 이게 아까우면 그냥 읽을 때 join하면 된다.

---

참고로 Flink에서는 이걸 regular join이라고 부른다 (PK, foreignkey 구분을 안 함) https://ci.apache.org/projects/flink/flink-docs-release-1.9/dev/table/streaming/joins.html#regular-joins https://stackoverflow.com/questions/57118053/flink-dynamic-table-vs-kafka-stream-ktable
