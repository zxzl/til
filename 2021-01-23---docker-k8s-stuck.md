---
template: post
slug: /docker-k8s-stuck
title: "맥 도커에서 쿠버네티스가 안 뜰 때"
author: "Hyeungshik Jung"
date: 2021-01-23 15:51:15
category: "til"
draft: false
---

minikube보다 도커에서 제공하는 로컬 쿠베가 훨씬 편한거는 맞는데 가끔 안 뜨는게 문제다.
맨날 찾다가 까먹어서 링크만 붙여 놓는다.

```sh
rm -rf ~/Library/Group\ Containers/group.com.docker/pki/
```

https://github.com/docker/for-mac/issues/3594
