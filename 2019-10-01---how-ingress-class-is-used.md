---
template: post
slug: /how-ingress-class-is-used
title: "Ingress 컨트롤러는 Ingress 설정을 어떻게 읽어올까?"
author: "Hyeungshik Jung"
date: 2019-10-01 22:45:44
category: "til"
draft: false
tags:
- k8s
---

* 오후에 회사에서 어떤 분이 Ingress 설정이 어떻게 Traefik 컨트롤러에 들어가는지 아냐고 여쭤보셨다. 당연히 Ingress 오브젝트의 `kubernetes.io/ingress.class: treafik`이 사용되지 않을까하고 얘기를 해보았는데 정작 컨트롤러 Pod 설정에는 `ingress.class`를 설정해주는 부분이 아무리 찾아봐도 없는 것이다. 물론 Traefik 컨트롤러의 `ingress.class` 기본값이 "traefik" 으로 되있다는 꽤나 합리적인 가설을 생각해볼 수 있지만, 그래도 어디선가 어떻게든 이 값이 사용은 되어야 한다.
* 결론부터 얘기하면 시스템 어딘가에 현재 사용가능한 ingress 컨트롤러 목록이 있는게 아니고, 그냥 각 traefik ingress controller pod은 k8s events 를 subscribe 하면서 그때그때 만약 ingress.class가 traefik으로 되어있는 ingress 설정이 있으면 그 설정에 맞게 내부 라우팅 설정을 변경하고, 자신에게 들어오는 요청을 알맞은 Pod에 전달할 뿐이다. 어디선가 shared state는 될 수 있으면 사용하지 않는 것이 복잡도를 줄인다는 얘기를 읽었는데 딱 그런 케이스인 것 같다.
* 사실 위와 같은 메모를 읽는 것보다 그냥 코드 읽는게 남는게 많다.
  * https://github.com/containous/traefik/blob/v1.7/provider/kubernetes/kubernetes.go#L160 클라이언트를 만들어서 쿠베 이벤트에 watch를 거는 부분
  * https://github.com/containous/traefik/blob/v1.7/provider/kubernetes/kubernetes.go#L205 ingress 설정 읽는 부분
  * 아예 읽기만 하지는 않고, ingress status를 update 한다. https://github.com/containous/traefik/blob/v1.7/provider/kubernetes/kubernetes.go#L439 kubectl describe ingress XXX 해서 나오는 내용이 여기를 거쳐서 업데이트 되는듯.
  * 이런 것도 다 테스트가 붙는데 (물론 겁나 길다..) 내가 다루는 수준에서 못 짠다는 것은 정말 핑계도 못된다고 느꼈다.
