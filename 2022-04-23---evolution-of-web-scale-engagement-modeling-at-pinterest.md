---
template: post
slug: /evolution-of-web-scale-engagement-modeling-at-pinterest
title: "Evolution of web-scale engagement modeling at Pinterest"
author: "Hyeungshik Jung"
date: 2022-04-23 00:40:02
category: "til"
draft: false
---

https://www.youtube.com/watch?v=C8H8PM5AB6A
을
NVIDA에서 한 행사에서 나온 톡인데 자기네들이 해온 것들 그리고 요새 하는 것들을 소상히 적어주고 있어서 재밌게 보았다. 결국 이 톡은 이 그림 한 장으로 요약이 된다.
![evoluion](./img/220423/1.png)
사실 톡을 보면 제일 좋은데 이 포스팅을 보시는 분들은 볼까 말까 고민하시는 분일 확률이 높으니 간단하게 정리해본다.


### 요약

* First ML model
    * 처음으로 ML 모델을 도입했고 간단한 logistic regression 모델이었다고 한다
* GBDT model
    * GBDT 모델을 도입하셔서 이전 모델에 비해 효과를 많이 보셨다고
* NN model
    * 처음으로 NN모델 시작.
* Deep Multitask NN model
    * NN모델을 더 딥하게. 레이턴시 이슈가 있으니 멀티태스크로 한 방에.
    * [How we use AutoML, Multi-task learning and Multi-tower models for Pinterest Ads](https://medium.com/pinterest-engineering/how-we-use-automl-multi-task-learning-and-multi-tower-models-for-pinterest-ads-db966c3dc99e)
* Realtime Features
    * 쉽게 얘기해서 방금 뭘 눌렀는지가 바로 피처로 들어가야 한다는 이야기
* PinSAGE
    * 서빙 타임에 모델에다가 유저/핀의 모든 히스토릭 데이터를 넣을 수 었으니 오프라인에서 미리 임베딩에 녹여놓고 모델에는 이것만 넣는 것이 효율적이다.
    * PinSAGE는 Graph Neural Network를 이용한 상품 임베딩
    * PinnerFormer는 (이름 참 잘 지었다) Transformer를 이용한 유저 임베딩
* Offline Exp Speedup
    * 단순한 mAP 이런거 말고 오프라인으로 더 실험 결과를 잘 예측할 수 있는 툴링이 생겼다는데 솔직히 이해를 잘 못했다.
    * [해당 블로그 포스팅](https://medium.com/pinterest-engineering/experiment-without-the-wait-speeding-up-the-iteration-cycle-with-offline-replay-experimentation-7a4a95fa674b) 읽어보고 내용을 업데이트 하겠다.
* Wide Networks
    * 기존에는 Representation / Summarization / Latent Cross 이런 식으로 Fully connected layer 전에 여러 단계가 있었는데, 유저/핀 임베딩을 쓰면 이게 비효율적이어서 그냥 Wide Fully connected layer로 바꾸셨다고.
    * 이러면 레이턴시가 느는데 quantization으로 잡으셨다.
* Transformer
    * DCNv2, PLE 같은 모델들이 하는 것을 결국 transformer에서 self-attention, multi-head로 다 하고, 실제로 성능도 더 좋았다. 대신에 레이턴시가 문제^^
    * GPU서빙 + 모델 최적화로 해결
* Sequence Features
    * 모델 용량이 더 커지니까 sequence 를 바로 모델에 넣을 수 있고, 효과가 좋았다고 한다
* Position Bias
    * Position도 트레이닝에 포함한 다음에 서빙타임에는 이걸 빼는 식으로 해결

### 느낀점

* Amazon에서 나온 [DCAF-BERT](https://www.amazon.science/publications/dcaf-bert-a-distilled-cachable-adaptable-factorized-model-for-improved-ads-ctr-prediction) 라는 페이퍼도 봤었는데 점점 CTR 모델, Engagement 모델 이런거도 transformer로 가는 것 같다. CTR 모델에 대해 공부를 하다보면 DCN, DIEN, CAN 등등 네트워크 구조가 엄청 많이 나오는데 서빙 인프라가 점점 발달하면 이런게 덜 중요해지지 않을까..?
* 온라인 서빙은 레이턴시에 예민하니까 어느 선까지는 PinSAGE, PinnerFormer 처럼 representation에 잘 투자하는 것이 서빙 인프라를 업그레이드를 하지 않으면서도 딥러닝의 덕을 볼 수 있는 방법 같다.
