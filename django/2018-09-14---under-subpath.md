---
title: Subpath에서 장고앱 돌리기 
slug: /under-subpath
date: "2018-09-14"
template: post
draft: false
category: "til"
tags:
    - "django"
---

[Zappa](https://github.com/Miserlou/Zappa)나 [Serverless](https://serverless.com/) 같은 툴로 장고앱을 디플로이하면 보통 `https://0c6zvbc5lk.execute-api.ap-northeast-2.amazonaws.com/dev/`처럼 URL의 Root가 아닌 subpath에 앱을 올려주는데, 이 상태에서는 장고앱이 라우팅을 잘 해주지 못한다. ( `/dev/`를 입력하면 `/`이 아니라 `/dev/`에 해당하는 뷰를 찾으려고 함)
이럴 때는 (배포용) 세팅에다가 
```
FORCE_SCRIPT_NAME = '/dev'
```
이런 식으로 subpath 명을 지정해주면 이 부분은 알아서 제외하고 라우팅이 정상적으로 이루어진다.  

p.s) 말끔한 프론트엔드 URL에 이렇게 못생긴 API URL을 붙이는 것이 프로토타이핑 할 때는 가장 적절한 것 같다 (API Gateway에 일일이 인증서를 붙이는 것도 일이다. 차라리 와일드카드 인증서 세팅이 된 kubernetes 클러스터를 하나 가지고 있으면 편하겠다 싶다).

## 참고 자료

* [How to host a Django project in a subpath?](https://stackoverflow.com/questions/28147916/how-to-host-a-django-project-in-a-subpath)
