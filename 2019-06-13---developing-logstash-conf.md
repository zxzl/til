---
template: post
slug: /developing-logstash-conf
title: "로그스태시 설정할 때 삽질을 빨리 하기"
author: "Hyeungshik Jung"
date: 2019-06-13 22:45:44
category: "til"
draft: false
tags:
- logstash
---

일을 하다 보면 종종 logstash 설정을 해야할 때가 있다 (fluentd가 좋다는 얘기는 들었는데 생각보다 ES랑 Kibana가 점점 강력해지면서 logstash를 계속 쓰게 되는 것 같다). 이게 평소 코딩할 때처럼 IDE에서 바로바로 경고를 해주는 것이 아니니까 매뉴얼만 보고 한 번에 원하는대로 로그를 파싱하기가 쉽지 않다. 결국은 설정을 해보고, 에러를 만나고, 그 에러를 고치는 식으로 작업이 진행되는데 이 때 로그가 레디스나 카프카를 통해 스트림에서 흘러오는 경우면 그냥 콘솔에 결과를 출력하게 하는 식으로 개발을 하면 되지만 파일 형태로 쌓이는 로그를 파싱할 경우 다시 시작해도 처음부터가 아니라 이전 offset부터 시작이 되서 빨리빨리 결과를 확인하기가 어렵다. 그럴 때는
* [--config.reload.automatic](https://www.elastic.co/guide/en/logstash/current/reloading-config.html) 옵션을 사용해서 설정 파일을 업데이트하면 바로 파이프라인이 재시작되도록 설정하고,
* offset이 저장이 되지 않도록 input을 설정하면
```
start_position => "beginning"
sincedb_path => "/dev/null"
```
중요한 케이스 몇 개만 샘플 파일에 넣어 놓고 에러 메시지를 보고 바로바로 설정을 고쳐가면서 결과를 확인할 수 있다. 
