---
title: psql의 비밀번호
slug: /pgsql-password
date: "2018-04-16"
template: post
draft: false
category: "til"
tags:
    - "docker" 
---

docker-compose 에서 postgreSQL 을 사용할 때, 환경변수로 비밀번호를 넣게 되어있다.

```yml
version: '3.1'

services:

  db:
    image: postgres
    restart: always
    volumes:
      - 'postgres:/var/lib/postgresql/data'
    environment:
      POSTGRES_PASSWORD: example
    ports:
      - "5432:5432"

volumes:
    postgres:
```

그런데 docker-compose.yml 에서 비밀번호를 바꾸고 다시 띄워도 postgres 에서 비밀번호가 바뀌지 않는다.
써놓고보니까 당연한데, 컨테이너가 시작될 때만 환경변수에서 비밀번호를 읽는거지 그 이후에는 내부적으로 volume 에 저장된다.  
진짜 써놓고 보니까 당연하네 ㅜㅜ더 자세한 내용은 [여기서](https://stackoverflow.com/questions/29580798/docker-compose-environment-variables)
