---
template: post
slug: /on-kafka-koh-kong
title: "카프카, 데이터 플랫폼의 최강자를 읽고"
author: "Hyeungshik Jung"
date: 2019-03-17 16:27:21
category: "til"
draft: false
tags:
- kafka
---

[네이버 도서 링크](https://book.naver.com/bookdb/book_detail.nhn?bid=13540082)

회사에 다니다보니 카프카를 안 쓸 수가 없어서 어떻게 익히지 하다가 회사 도서관에서 빌려본 책. 저자 두 분은 카카오에서 전사 공용 카프카 클러스터를 운영하시는 분들인데 그래서 그런지 정말 실용적인 내용만 쏙쏙 담아주셨다. 내용은 읽으면서 작성한 [노션 메모](https://www.notion.so/zxzl/kafka-553720ad75624cba8d63f7f5e7c5f63d)를 참고하시고... 전반적으로 엄청 큰 클러스터를 운영할 때의 꿀팁, 삽질기 그런 내용보다는 카프카를 처음 접하고 한 5대 규모로 운영해야 하는 개발자를 위해 쓰여졌다. 어쩌면 카프카가 이제 꽤 성숙해서 설명이 별로 없는 것일 수도 있겠는데 어쨌든 '주키퍼 깔고 카프카 깔고 토픽 만들고 프로듀서 만들어서 컨슈머로 가져다 쓰고,,, 모니터링도 하고 가끔 업데이트도 하고'하는 흐름을 충실하게 짚어주신다.

원래 책을 읽으면 예제를 다 따라 쳐보고, 결과도 다 확인하는 편인데 너무 설명이 잘 되있어서 직접 따라쳐보지 않고 3시간 동안 빠르게 읽었다. 그렇게 읽어도 좋고, 실제 머신에 하나씩 깔아보면서 따라가도 좋은 책. 역서보다도 이렇게 한국분이 쓰신 좋은 책을 보면 참 반갑다.

다시 원래 목적으로 돌아와서 카프카를 '익히려면' 이제 무엇을 해야할까. 제일 좋은 것은 이걸 이용한 무언가를 만들어보는 것인데 그러려면 일단 카프카를 띄워야 한다. 내가 카프카를 리얼 머신에 깔면서 운영할 일은 없으니 (깔아도 아마 k8s에 깔 듯...?) 간편하게 도커로 띄워보자. 이광식님의 [블로그 글](http://www.kwangsiklee.com/2017/03/docker-compose%EB%A1%9C-kafka%EB%A5%BC-%EB%A1%9C%EC%BB%AC%EC%97%90-%EB%9D%84%EC%9B%8C%EB%B3%B4%EC%9E%90/)이 설명이 가장 잘 되어 있지만, 한가지 아쉬운 점은 kafka-manager가 빠져있다는 것이다. 마찬가지로 두가지 방법이 있다
1. 소스를 받아서 로컬에서 빌드하는 방법 - 아마 같은 층에 계신거로 추정되는 분이 정리를 잘 해주셨다. [https://blog.naver.com/occidere/221395731049](https://blog.naver.com/occidere/221395731049)
2. 도커를 쓰는 법 - 다행히 도커로 잘 말아주신 분이 계신다. [sheepkiller/kafka-manager-docker](https://github.com/sheepkiller/kafka-manager-docker)

(19-04-06) 아래와 같은 삽질을 하지 말고 그냥 [Confluent Platform Quickstart 도커 이미지](https://docs.confluent.io/current/quickstart/ce-docker-quickstart.html)를 쓰면 해결

카프카 매니저만 따로 띄우면 계속 올리고 내리기 귀찮으니까 저기서 나온 `docker-compose-single-broker.yml` 밑에다가 kafka-manager를 추가해준다.
```docker
(기타 등등)
kafka-manager:
  image: sheepkiller/kafka-manager:latest
  ports:
    - "9000:9000"
  links:
    - zookeeper
    - kafka
  environment:
    ZK_HOSTS: zookeeper:2181
    APPLICATION_SECRET: letmein
    KM_ARGS: -Djava.net.preferIPv4Stack=true
```
이렇게 하면 왠만한건 다 되는데, 저 이미지가 빌드된지 좀 되서 최신 kafka-manager가 담겨있지 않다. 따라서 최신 버전의 카프카를 지원하지 않고, 몇몇 [이슈](https://github.com/yahoo/kafka-manager/issues/407) 들을 밟을 수 있으니 저 이미지를 살짝 고쳐서 최신 버전의 kafka-manager 이미지를 만들어야 한다.
[`sheepkiller/kafka-manager-docker/Dockerfile`](https://github.com/sheepkiller/kafka-manager-docker/blob/master/Dockerfile)를 보면 
```
ENV JAVA_HOME=/usr/java/default/ \
    ZK_HOSTS=localhost:2181 \
    KM_VERSION=1.3.1.8 \
    KM_REVISION=97329cc8bf462723232ee73dc6702c064b5908eb \
    KM_CONFIGFILE="conf/application.conf"
```
이렇게 환경 변수를 설정해주는 부분이 있다. 여기서 `KM_VERSION`이랑 `KM_REVISION`을 수정해주면 된다. kafka-manager 리포의 [release](https://github.com/yahoo/kafka-manager/releases)를 보면 최신 버전이랑 커밋ID를 알 수 있다. 그걸로 환경 변수를 업데이트하고, 
```
ENV JAVA_HOME=/usr/java/default/ \
  ZK_HOSTS=localhost:2181 \
  KM_VERSION=1.3.3.23 \
  KM_REVISION=2ca848bfdf542bf1da8fc860db9bbcc99548f89d \
  KM_CONFIGFILE="conf/application.conf"
```
**영겁**의 세월을 기다려서 이미지를 빌드하면 개발용으로 쓸만한 카프카랑 카프카 매니저를 편하게 띄울 수 있다.
