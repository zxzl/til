---
template: post
slug: /google-cloud-next--extended--yangjae
title: "Google Cloud Next 2019 Extended를 다녀와서"
author: "Hyeungshik Jung"
date: 2019-04-27 19:23:08
category: "til"
draft: false
tags:
- cloud
---

주말에 뭐하지 하다가 festa.io에서 표를 팔길래 한 번 가봄. 행사 관련 정보는 행사 페이지 - [Next '19 Extended | Festa!](https://festa.io/events/256) 를 참조.

### Beyond kubernetes (조대협님)
* microservice pattern
	* eg) circuit braking. 
		* Netflix OSS hystrix 등
			* 다 좋은데 Application Layer고, Java로 되있다
			* 스프링이 좀 편한데 그래도 자바다
		* Envoy
			* Network Layer에서!
			* 다 좋은데… 그때그때 계속 어플리케이션을 엮기가 귀찮다
* Istio
	* 그냥 k8s 쓰듯이 쓰면 컨테이너에 Istio Proxy가 알아서 붙음
	* Traefik이랑 크게 다른건 없는데 정책을 입힐 수 있다는 것이 큰 차이점
	* 특히 눈에 띄는 기능
		* Rate control이 특히 눈에 띄었음
		* 네트워크토폴로지 뽑아줌
		* Distributed Tracing도 된다
	* 다 좋은데 깔아서 쓰면 힘드니 managed service를 쓰자  (GKE씁시다)
	* 1.0 나옴
* DevOps
	* 개발자들이 스스로 운영을 한다
	* DevOps팀은 그게 가능하도록 시스템을 만든다
	* DevOps 팀의 목표는 DevOps 팀이 필요 없는 것인데…그게 잘 안 됨
* SideCar  패턴
	* 보통 App이 있고 fluentd, envoy 이런게 같이 붙는다
	* 어차피 App이 바뀌는거니까 그냥 Pod 하나에 다 놓고 앱만 바꾼다
	* 괜히 k8s에서 Pod 하나에 컨테이너 여러개 들어가 있는게 아니다
* Spinnaker
	* 클러스터 환경에서 배포 / 롤백 프로세스를 자동화해줌
		* 스모크 테스트를 하고 30분동안 괜찮으면 넘어가자
		* 밤에만 배포를 하자
		* 자동 롤백
* StackDriver
	* 장비 레벨, 컨테이너 레벨, 어플리케이션 레벨의 모니터링이 한 방에!
	* 라우팅 용도로 쓰고 저장은 BigQuery에 하면 비용이 겁나 싸다
		* 모 회사는 오픈소스 기반으로 하다가 (NELO 같은 툴이 있었나 봄) 이거로 바꿔서 비용 1/10으로 줄이고 운영인력 다 필요 없어짐
	* 네이버 / 카카오 사이즈 아니면 직접 모니터링 하지 말고 DataDog이나 기타 제품을 써라
		* 예전에 Heapster에서 Prometheus로 훅 넘어간 적이 있다. 이 리스크를 작은 조직이 감당하기는 부담이 된다
	* 앱간의 관계를 그래프로 그려준다. 분산 트레이싱도 당연히 지원 (Zipkin, Jagger 기반. 스프링에서 쉽게 붙일 수 있음)
* Knative
	* PaaS의 일종
	* 쿠베 오케스트레이션이 춘추전국 시대를 거처서 K8s로 왔듯이 PaaS 솔루션도 이게 대세가 될 느낌이 온다
	* 다른건 불안한데 Run + Deployment 까지는 그래도 안정화가 되어있다
* Kubeflow
	* 정말정말 좋은데, 아직은 쓰면 안된다
	* ML은 모델보다도 데이터 - 빌드 - 서빙 등등 파이프라인 구축이 겁나 빡세다. 
	* K8S 기반
	* Jupyter Labs라고 공용 Jupyter Notebook이 있다
	* TF 분산 환경 설치에 장점이 있다
	* Seldon 이라고 TF serve 비슷하게 여러 프레임워크 지원하는 서빙용 엔진도 지원
* Anthos
	* GKE를 고객사 On premise 머신에 깔아드림
	* AWS, Azure에도 올라감
	* k8s 운영을 다 해줌
	* 비쌈
* 기타
    * 여기 나온 툴, 그러니까 CNCF에서 한창 개발되고 있는 툴들은 1.0 버전이 나오기 전에는 쓰는 것을 비추. API가 정말 훅훅 바뀐다.
	* 그리고 실제 얘네 운영하려면 literally 깊은 빡침을 느낄 수 있음. (PPL) 구글 클라우드 쓰시라.
* (글쓴이 주)
	* GCP가 레퍼런스가 적어서 그렇지 특히 컨테이너 쪽으로 좋은게 많은듯…확실히 k8s 원조 맛집.

### Knative로 서버리스 워크로드 구현 (김진웅님)
* Managed FaaS
	* Vendor lock-in의 문제
* FaaS on k8s
	* 한 15개 있다
	* Knative가 나온지는 얼마 안 됬는데, 겁나 유망하다
* Knative
	* 빌드 - 배포 - 관리를 하나로
	* 소스 중심, 컨테이너 기반이라서 언어 프레임워크를 가리지 않음
	* 서비스 디스커버리 용으로 Istio나 Gloo가 있어야 한다
	* Eventing이 좀 유연한거 같다
		* 이거는 그냥 Service-Deployment-Pod에 비해 장점이 될 거 같음
* (글쓴이 주 1) 얘는 스케일링이 팍팍 잘 될까?
	* [Autoscale Sample App - Go | Knative](https://knative.dev/docs/serving/samples/autoscale-go/index.html)
	* 사실 k8s의 CPU 기반의 HPA가 굼떠서 좀 못마땅하고 불안했는데 (기본값은 15초 주기로 CPU 사용량의 평균을 냄) 
	* 얘는 일단 1분 윈도우를 가지고 Pod별 concurrency를 계산해서 target concurrency랑 맞추고 (다만 이건 개발자가 정해야되니까 좀 더 정하기 어려울 수 있겠다)
	* 또 패닉 모드라고 있어서 갑자기 트래픽이 확 치고 올라오면 바로 Pod 갯수를 2배로 올리는 기능이 있음
	* HPA 보다는 확실히 옵션이 좋다
* (글쓴이 주 2)
	* 한동안 AWS 람다에서 이것 저것 돌리려고 AMI Linux에서 빌드하고 그 아티팩트를 람다에 태우는 방법들을 많이 찾아봤는데, 이제 그것도 옛날 얘기가 될거 같다

### Cloud Spanner (이정운님)
* CAP를 진짜 다 충족 하느냐?
	* CAP 이론을 창시하신 분 (now at Google)이 쓰신 [Spanner, TrueTime & The CAP Theorem](https://ai.google/research/pubs/pub45855) 을 읽어보면 얼추 다 충족한다고 되있다
* 99.999% SLA 경쟁 DB보다 높음
	* 이미 구글에서 5년 넘게 사용함
* 엄청 빠르지는 않지만 노드가 추가되는 와중에도 지속적인 성능을 보여줌
	*  [Quizlet Tests Cloud Spanner — The Most Sophisticated Cloud Database | Quizlet](https://quizlet.com/blog/quizlet-cloud-spanner)
* 스키마 변경, 트리거, 프로시저 많은 경우는 별로
* 사례
	* 에버노트
	* Streak - Next 19 https://www.youtube.com/watch?v=3aHBkfBRFEU
	* 게임 - 
		* 글로벌 런칭하면 레이턴시, 노드 갯수 등등 관리가 손이 많이 간다
		* 그리고 게임이 오픈빨이 빠지고 접속자가 줄면 노드를 엄청 쉽게 줄일 수 있다
		* [Colopl open sourced a Cloud Spanner driver for Laravel framework](https://medium.com/google-cloud/colopl-open-sourced-a-cloud-spanner-driver-for-laravel-framework-4ca1db018a3)
* 오로라랑 비교
	* 오로라는 MySQL compliant
	* 얘는 아니다
	* 대신 수평 확장에 구조가 더 유용하다 (오로라는 최근에 마스터 - 마스터 레플리카가 됐는데 얘는 애초에 그렇게 만들어짐. 근데 얘네들은 지연이 거의 없어서 레플리카가 레플리카가 아님...)
* (글쓴이 주)
	* New SQL 이라고 TiDB, CockroachDB 이런게 막 나오는데 아직 우리나라 사이즈에서는 정말 정말 transaction 필요한 건 그냥 어플리케이션 샤딩을 해버리고 나머지는 NoSQL 쓰는게 더 싸게 먹히고, 쌓여있는 경험도 많은거 같음
	* 저게 되려면 일단 인프라 단에서 스토리지도 좋고 네트워크도 좋고, 일단 시계가 좋아야 한다.. 구글이 괜히 오픈소스를 하는게 아니다
