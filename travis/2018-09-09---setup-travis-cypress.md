---
title: Cypress + Travis + Webpack
slug: /setup-travis-cypress
date: "2018-09-09"
template: post
draft: false
category: "til"
tags:
    - "travis"
    - "cypress"
---

* [Cypress](https://www.cypress.io/)라는 툴로 Integration test를 붙이고 싶은데, 기왕 하는김에 [Travis CI](https://travis-ci.org/)에서도 테스트를 돌리고 싶음
* 친절하게 [설명서](https://docs.cypress.io/guides/guides/continuous-integration.html#Travis)를 따라가면 될 것 같지만, 저건 Cypress를 돌리는 얘기고 Cypress가 테스트할 웹사이트는 띄워지지 않음
* 다른 [예제](https://github.com/cypress-io/cypress-example-todomvc/blob/master/.travis.yml)를 보면 백그라운드 프로세스로 npm start를 해주는데 여기서 npm start는 dev server가 아니라 이미 빌드된 애를 서빙만 해주는 [http-server](https://www.npmjs.com/package/http-server)를 띄우는 스크립트임에 유의해야 한다.
    ```yml
    before_script:
    - npm start -- --silent &
    ```
* webpack dev server 등을 띄우면 빌드가 완료되서 웹앱을 서빙하기 이전에 Cypress 테스트가 시작되서 다 깨진다ㅜㅜ. `index.html`이 빈 페이지인데 무슨 테스트를 하겠어. 이 문제 때문에 [wait-on](https://www.npmjs.com/package/wait-on) 이라는 패키지도 써보고 이런저런 시도를 많이 했는데, 결국 빌드를 하고 정적파일을 호스팅해서 그 주소로 테스팅을 진행하는 방식을 택했다.
* 생각해보면 이게 좀 더 실제 환경이랑 가까우니까 개발서버로 테스팅이 가능했어도 이렇게 하는게 맞다.
* 개인적으로는 가볍게 정적파일을 서빙할 때 [serve](https://www.npmjs.com/package/serve)를 더 좋아해서 최종적으로 이렇게 생긴 [`.travis.yml`](https://github.com/wafflestudio/snutt-webclient/blob/master/.travis.yml)을 작성했다.
    ```yml
    before_script:
    - yarn build # 이러면 dist에 index.html 등등해서 생김
    - serve -s dist &
    script:
    - cypress run --record
    ```
