---
title: Pyenv 설치시 의존성
slug: /pyenv-dependency
date: "2018-04-22"
template: post
draft: false
category: "til"
tags:
    - "python"
---

Pyenv 자체는 금방 깔리는데 막 띄운 인스턴스에는 빌드 디펜던시가 하나도 없음. Pyenv 는 파이썬 소스를 가져다가 빌드하는 방식이라서 툴들이 필요함.
[여기](https://github.com/pyenv/pyenv/wiki/Common-build-problems)를 따라서

```
sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev \
libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
xz-utils tk-dev
```

를 쭉 깔아준다.
