---
title: 다양한 isolation level
slug: /various-isolation-levels
date: "2019-02-17"
template: post
draft: false
category: "til"
tags:
    - "database"
---

요즘 [Designing Data Intensive Application](https://www.amazon.com/Designing-Data-Intensive-Applications-Reliable-Maintainable-ebook/dp/B06XPJML5D/ref=sr_1_1?crid=2H5GZ7J4G1UC9&keywords=designing+data-intensive+applications&qid=1550406040&s=gateway&sprefix=designing+data%2Caps%2C341&sr=8-1)이란 책을 읽는데 Transaction 챕터는 메모를 하면서 읽어서 공유해 봄. 그림을 그대로 복붙한 것이 맘에 걸리는데...부디 그림을 보시고서 '아 좋은 책이구나' 하고서 많이 사셨으면 좋겠음.

https://www.notion.so/zxzl/Ch7-Transaction-de5a5326bf58462e8b686689e1b56e64
