---
title: Python Crawling with Serverless framework
slug: /serverless-crawling-python
date: "2018-10-11"
template: post
draft: false
category: "til"
tags:
    - "python"
    - "lambda"
---

사실 너무 당연한건데 안 써놓으면 까먹으니까.

### Serverless + Python 관해서
* 프로젝트별로 virtualenv를 만들어서 시작한다. 나중에 requirements.txt를 만들어야 의존성을 패키징할 수 있는데, 원래 쓰던 환경에서는 온갖 라이브러리가 다 깔려있을테니 `pip freeze > requirements.txt`로 파일을 작성할 수가 없다.
* `sls invoke local`을 최대한 써먹어서 적어도 로직 자체의 오류 때문에 deploy를 다시 할 일은 없게 만든다. 
    - [공식 가이드](https://serverless.com/framework/docs/providers/aws/guide/testing/)에서는 유닛 테스팅은 언어별 테스팅 프레임워크를 활용하고, 인테그레이션 테스팅 용으로 invoke를 사용하라고 나와있다.
* 사용하는 라이브러리가 C의존성이 있다면, serverless-python-requirements 패키지의 [dockerizePip](https://github.com/UnitedIncome/serverless-python-requirements#cross-compiling) 옵션을 꼭 켜준다.
    - 이번에 크롤링을 할 때는 lxml 패키지가 그랬는데 저걸 모르고 약간의 삽질을 거쳤다.
* [requests-html](https://html.python-requests.org/) 진짜 너무 편하다. `bs4`는 이제 안녕.

### 크롤링 + 람다에 관해서
1. 별도로 동시성 옵션을 지정해주지 않으면 금새 동시에 1000개의 람다 컨테이너가 해당 사이트를 크롤하게 되는데, 그러다보면 사이트가 마비되어 크롤은 실패하고, 람다 함수는 timeout 되서 끝나고, 함수는 오래 실행되었으니 요금은 많이 청구되는 속상한 경우가 생긴다. 꼭 적절한 숫자의 동시성을 설정해주자.
2. Invoke Async vs Synchronous
    * Async 방식으로 함수를 실행하면 적어도 랩탑에서의 파이썬 코드가 바틀넥이 되지는 않는데 대신 결과를 SQS, S3, DynamoDB 등에 저장을 해야한다. 결과까지 돌려 받는 식으로 Synchronous Invoke를 받으면 여러 개의 람다 컨테이너로부터 결과를 받는 부분이 바틀넥이 될 수 있는 대신 (그래도 로컬 CPU의 쓰레드를 활용하는거 보다는 훨씬 동시에 실행할 수 있는 작업의 개수가 많다) 평소에 하던대로 로컬에서 데이터 전처리를 마저 할 수 있다. 시간이 오래 걸리는 부분만 람다로 짜는 것도 방법이다.
    * 양이 진짜 많은 경우 람다 여러개로 전처리를 하고 Kinesis에 넣어서 최종적으로는 S3에 쓰기 편한 결과물이 저장되는 것이 이상적이겠다. 로컬 <-> AWS 사이의 데이터가 오가면 비용과 시간이 많이 들테니까.
3. 어쨌든 Fail하는 케이스가 생기는데, Async로 Invoke할 경우 이런 실패한 태스크들을 어떻게 처리해야하는지 아직 맘에 드는 방법을 못찾겠다. 데이터베이스를 쓰는 것이 답일까.
