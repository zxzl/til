---
title: 장고 정적 파일을 S3로 관리하기
slug: /collectstatic-s3
date: "2018-04-28"
template: post
draft: false
category: "til"
tags:
    - "django"
---

Django 프로젝트를 별도의 서버에서 Deploy 하는 대신 Zappa 로 deploy 하려고 하니까 static 파일들을 다른 곳에서 serve 할 필요가 생겼다.
그냥 라이브러리 써서 간단히 될 것 같은데 직접 해보면 은근히 시간이 걸리니 이참에 정리해놓자.

## S3 에 collectstatic 하기

기본적으로는 django-storages 의 [설명](http://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html)을 따라감.

*   직접 버킷을 만들면 권한 설정이 까다로우니 그냥 `AWS_AUTO_CREATE_BUCKET` 옵션을 이용하자.
*   버킷이 만들어진 후 `python manage.py runserver --nostatic`으로 개발서버를 띄워서 static 애셋이 잘 로딩되나 보자.

예전에는 AWS Signature Version 4 때문에 복잡했다는데 지금은 그냥 seamless 하게 잘 된다.

## 참고 자료

*   https://blog.leop0ld.org/posts/django-use-s3/
