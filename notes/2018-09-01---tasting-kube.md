---
title: Kubernetes에 관한 잡생각
slug: /tasting-kube
date: "2018-09-01"
template: post
draft: false
category: "til"
tags:
    - "notes"
---

* 튜토리얼을 두어개 따라가봤음
	* [Example: Deploying PHP Guestbook application with Redis - Kubernetes](https://kubernetes.io/docs/tutorials/stateless-application/guestbook/)
	* [Example: Deploying WordPress and MySQL with Persistent Volumes - Kubernetes](https://kubernetes.io/docs/tutorials/stateful-application/mysql-wordpress-persistent-volume/)

* AWS
	* AWS에 다양한 서비스를 조합해서 사용하는게 생각보다 어려워보인다
	* 띄우는건 어렵지가 않은데 얘네를 통합된 관점에서 관리하는게 어렵다
	* 그래서 회사들은 TerraForm 같은 친구를 사용하겠지
	* 근데 여기도 결국 AWS에 종속된다는 문제가 생김 (회사 관점에서는 문제겠다...)
* Vendor-agnostic
	* 그냥 아무 클라우드에나 하나 클러스터를 만들어 놓으면 Kubernetes 운영 노하우를 써먹을 수 있어
		* 공통된 정의로
		* 그래서 이게 거의 업계 표준이 되는 이유겠지. AWS를 쓰나 자기 서버를 쓰나 똑같으니까
* 용도
	* DB 같이 장애가 나면 큰일나는 서비스는 Managed 서비스를 쓰는게 좋은거 같은데 아닌 경우라면 깔아서 쓰는게 더 저렴하고, 잉여자원을 활용하기 좋을거 같아
	* 특히 개인이 이것저것 스택을 조합하는 연습을 할 경우 이렇게 Unified된 방법으로 이런 저런 도구를 띄었다 내렸다하면서 쓸 수 있다는게 엄청 매력.  특히 기본적으로 여러 노드에 올려서 써야하는 도구들의 경우 장점이 클 거 같다.돈은 따로 더 안나가고
		* 약간만 익혀놓으면 생산성이 크게 늘 것 같아
* 어떻게 익히지?
	* 항상 그렇지만, 자기가 직접 써봐야 는다고 생각해
	* Minikube에라도 이것저것 올리는 연습을 하면 좋지 않을까
	* 괜찮은 과제
		1. 지금 개인 홈페이지는 Next.js로 정적 사이트를 만들어서 Netlify에서 서브하는 형태로 되있는데, ~~얘를 SSR을 해주는 도커 이미지~~정적 파일을 serve하는 [nginx 이미지](https://gitlab.com/zxzl/research-website/blob/master/Dockerfile)로 만들어서 얘를 다시 쿠베에 올리는 연습을 하면 꽤 해볼만 하지 않을까?
            * 생각보다 인증서 붙이고 하는 과정에서 AWS / GCP를 사용할 때랑 Minikube에 올릴 때랑 많이 다른 것 같다.
            * ~~어차피 쓸 일이 있으면 AWS에서 쓸테니 kops로 작은 클러스터를 띄우는게 맞는 공부 방향 같다. Spot으로 띄우면 얼마 안하지 않을까?!~~ 보니까 GKE는 아예 Preemptible VM + Auto Scaling 으로 만들 수 있는 옵션이 있다. 
            * 보니까 손으로 일일이 버전업을 하는거는 kubernetes를 쓰는 의미가 없고.. Helm 차트로 만들어서 업데이트 하는 방법이 맞는거 같다.
            * 클러스터를 판게 아까워서라도 천천히 옮겨봐야겠다.
	* 논문을 다 내고 해볼만한 태스크 같아. 항상 데드라인이 있을 때 이런게 재밌더라.
