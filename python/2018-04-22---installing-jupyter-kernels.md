---
title: Jupyter 커널 추가하기
slug: /installing-jupyter-kernels
date: "2018-04-22"
template: post
draft: false
category: "til"
tags:
    - "python"
---

걍 한 줄인데 계속 검색하게 됨 ㅜㅜ
[링크](https://ipython.readthedocs.io/en/latest/install/kernel_install.html)

```
python -m ipykernel install --user --name myenv --display-name "Python (myenv)"
```
