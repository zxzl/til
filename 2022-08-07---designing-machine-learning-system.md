---
template: post
slug: /designing-machine-learning-system
title: "Designing Machine Learning System을 읽고"
author: "Hyeungshik Jung"
date: 2022-08-07 18:40:02
category: "til"
draft: false
---

https://www.amazon.com/Designing-Machine-Learning-Systems-Production-Ready/dp/1098107969

ML을 공부한다하면 일단 정제된 train/validation/test 데이터를 가지고 이리저리 모델을 바꿔가면서 metric을 뽑아먹는 것으로 생각하기 쉬운데 실제 회사에서 돈되는 문제에 ML을 적용하려면 신경을 쓸 것이 많다. 일반적인 소프트웨어 엔지니어링에 비해 역사가 짧다 보니까 아직 어떤게 '잘'하는 것인지 사람마다 생각이 다른 와중에 저자는 본인의 경험이랑 네트워크랑 이것저것 조합해서 나름 comprehensive한 가이드를 책의 형태로 엮어냈다.

개인적으로 좋았던 파트들을 꼽아보면

4. Training Data
5. Model Development and Offline Evaluation
8. Data Distribution Shifts and Monitoring

이었다. 기본적인 소프트웨어 엔지니어링 / 데이터 엔지니어링 다룬 부분들도 내용이 틀린 말은 없는데 아무래도 내게 친숙한 내용이 많아서 위에서 뽑지는 않았다 (SWE 이외의 배경을 가지신 분들에게는 좋은 정리가 될 수도 있겠다 싶다)

소프트웨어 툴들은 보면 성숙해지면서 점점 '대세' 툴들이 생기기 마련이다. ML에서도 이런 식으로 best practice가 정리가 되면 점점 이를 반영한 툴들이 나올 것이고 그런 툴들이 업계의 평균 수준을 올려낼 것이다. 그럼에도 불구하고 비즈니스에 맞게 데이터 수집 / 모델 훈련 / 모니터링 / 재훈련 이 flow를 만들어내고 관련 stakeholder들과 커뮤니케이션 하는 것은 절대 상용화된 툴이 해줄 수가 없다. 이 때 엔지니어의 경험과 재능이 필요하다고 본다.

p.s) 인더스트리 경험이 엄청 긴 것도 아니고, 박사가 있는 것도 아닌데 이런 책을 쓸 수 있는 저자가 참 대단하다고 느꼈다.
