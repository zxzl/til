---
title: AWS Batch로 Gensim 돌리기
slug: /awslinux-gensim
date: "2018-10-28"
template: post
draft: false
category: "til"
tags:
    - "docker" 
    - "python"
---

Gensim을 돌리다보면 램이 모자랄 때가 많아서 EC2를 잠깐 띄워서 돌렸었는데 이것도 하다보니까 반복인 것 같아서 AWS Batch를 써보기로 했다. 기본적으로는 예제로 나오는 [Fetch & Run](https://aws.amazon.com/blogs/compute/creating-a-simple-fetch-and-run-aws-batch-job/)이랑 크게 다를게 없는데 태스크를 돌릴 이미지를 만들 때 실수하기 쉬운 부분이 있다. 사실 스크립트로 EC2 띄우는 거랑 크게 없지만 요구하는 사양에 맞추어서 적절하게 스팟 인스턴스로 돌려주고, 또 띄우고 내리는 과정이 자동이라서 이참에 써보기로 했다. 다음은 이미지를 만들 때 유의할 점.

* `amazonlinux` 이미지 위에다가 만들 것. 처음에는 `python:3.6` 이미지로 했다가 나중에 CloudWatch에서 로그를 확인하기 위해서 awslogs를 깔아야 함을 깨닫고, 깔려다가, 인스톨러는 파이썬 3.6을 지원 안하고...그냥 아마존 리눅스에서 까는게 더 낫겠다 해서 베이스 이미지를 바꿨다.
* gensim을 깔기 전에 컴파일러들이랑 python-devel도 같이 깔아줄 것. 이거 없으면 C컴파일을 안해서 FAST_VERSION 값이 1로 안나오고, 트레이닝이 느릴꺼라고 경고가 나온다. `python:3.6` 이미지에서는 그냥 잘 되었는데 `amazonlinux`에서는 설치가 필요했다.
* 이것만 주의하면 그냥 리눅스다. 삽질 끝에 정착한 Dockerfile은 다음과 같다.

```Dockerfile
# Dockerfile
FROM amazonlinux:latest

# Ubuntu에서 build-essential 비슷한 것
RUN yum groupinstall -y "Development Tools"

RUN yum install -y python3
RUN yum install -y python3-devel

RUN pip3 install gensim
RUN pip3 install awscli
RUN python3 check_fast_version.py
```

```python
# check_fast_version.py
import sys

try:
    from gensim.models.word2vec_inner import FAST_VERSION

    print('FAST_VERSION ok ! Retrieved with value ', FAST_VERSION)
    sys.exit()
except ImportError:
    print('Failed... fall back to plain numpy (20-80x slower training than the above)')
    sys.exit(-1)
```

#### 느낀점
* 애초에 개발을 도커 위에서 해야지 로컬에서 해보고 옮기려고 하면 시간을 너무 많이 쓴다. 어떤 선배 블로그에서 모든 로컬 개발 환경을 도커로만 구성한다고 읽었는데 무슨 의미인지 알겠다.
* s3에서 데이터를 가져오는 경우에는 애초에 개발을 AWS 인스턴스 위에서 해도 좋겠다.
* 로컬에서 램이 모자라서 안 돌아가던거도 그냥 코어 / 용량을 패러미터로 넣어 주면 알아서 인스턴스가 생기고, 일이 끝나면 다시 꺼지니까 너무 편하다 +_+
