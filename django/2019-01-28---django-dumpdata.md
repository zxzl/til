---
title: 장고에서 manage.py로 데이터 옮기기
slug: /django-dumpdata
date: "2019-01-28"
template: post
draft: false
category: "til"
tags:
    - "django"
---

장고 앱을 다른 곳에서 띄울 때는 그냥 `pg_dumpall` 등의 툴을 사용해서 데이터베이스 스키마랑 데이터랑 싹 옮겨서 새 서버에서는 마이그레이션도 필요 없게 썼었는데, 오늘은 pg_dumpall이 중간에 계속 멈추는 상황이 있었다. 그래서 Postgres의 백업 기능 말고 장고의 데이터 내보내기 / 불러오기 기능을 써보기로 했다.
기본적으로

```bash
(기존 디비에 물린 상태)
python manage.py dumpdata --exclude=contenttypes --exclude=auth.Permission > 파일.json
```

```bash
(새 디비에서 마이그레이션 돌리고 난 후 데이터는 없는 상태)
python manage.py loaddata 파일.json
```

을 돌리면 잘 되었다. 처음에 `contenttypes`, `auth.Permission` 테이블을 제외하는 것을 몰라서 약간 헤맸다.

### 도움이 된 글

-   [Django fixture dumpdata, loaddata and Integrity Error](https://coderwall.com/p/kogbla/django-fixture-dumpdata-loaddata-and-integrity-error)
