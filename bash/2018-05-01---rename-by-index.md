---
title: 순서를 이용해서 파일 이름에 숫자 붙이기
slug: /rename-by-index
date: "2018-05-01"
template: post
draft: false
category: "til"
tags:
    - 'bash'
---

https://stackoverflow.com/questions/3211595/renaming-files-in-a-folder-to-sequential-numbers

파일 이름을 그냥 싹 순서로 바꾸고 싶을 때

```bash
ls | cat -n | while read n f; do mv "$f" `printf "%04d.extension" $n`; done
```

## 해설

```bash
$ ls
bash     caddy    django   docker   leetcode python
```

```bash
$ ls | cat -n
     1	bash
     2	caddy
     3	django
     4	docker
     5	leetcode
     6	python
```

`while read n f` 이거는 튜플 읽듯이 라인넘버랑 파일명을 파싱해서 루프 안에 집어 넣어주는거
