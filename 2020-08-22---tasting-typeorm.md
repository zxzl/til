---
template: post
slug: /tasting-typeorm
title: "TypeORM 구경하기"
author: "Hyeungshik Jung"
date: 2020-08-22 23:22:20
category: "til"
draft: false
---

### vs sequelize
* 부스트캠프 리뷰어 지원하고나서 sequelize 를 좀 보다가 너무 맘에 안들어서 TypeORM 을 보고 있는데 훨씬 맘에 들었다.
* 타입스크립트 지원이 훨씬 잘 되고, migration을 자동으로 만들어주는 것도 맘에 든다 (시퀄라이즈는 그런거 없고, 써드파티 툴이 하나 있다)

### Relation
* 사실 ORM이 스키마 잘 만들어주고 get/set 잘 해주고 validation 잘해주면 됐지 그 이상은 바라지도 않는다
  * TypeORM은 그걸 잘 해주고 있고, 이정도면 몽고디비보다 개발 속도가 절대 느리다고 할 수가 없다. 
  * 몽구스 이런거로 엔티티 만드는거보다 어쩌면 더 빠를 수 있다.
* relation을 가지고 쿼리 만들 때 좀 난감했는데, 이건 그냥 빠르게 포기를 하고 sql로 썼다.
  * https://erikbern.com/2018/08/30/i-dont-want-to-learn-your-garbage-query-language.html 이 분 말처럼 쿼리가 좀 길어지면 그냥 sql로 쓰는게 누구나 알아보기 편하다.
* relation을 만들면 FK가 걸려서 인덱스가 생기는게 그게 너무 아까웠다.
    * 좋아요 테이블을 (postId, userId) 에 PK걸어서 쓰면 될 것을 postId, userId 에도 인덱스 걸리는게 너무 아까워서 그냥 relation을 안걸었다. 
    * 어차피 좋아요 갯수 세서 데이터 가져올 때는 데이터로더로 여러 게시물에 대해서 한번에 가져오니 릴레이션이 하나도 아쉽지 않다.
* 그리고 규모가 커지면 어차피 디비 말고 이곳 저곳에서 데이터를 가져오게 되있고 결국에는 조인을 다 풀어야 한다.

### (DB를 포함한) 테스트 돌리기
* 테스트 디비를 별도로 띄우기가 귀찮아서 https://gist.github.com/Ciantic/be6a8b8ca27ee15e2223f642b5e01549 을 참고해서 테스트시에는 sqlite3를 잠깐 띄워서 테스트하도록 해봤다
* 당연히 왠만한건 다 돌아가는데 createdAt, updatedAt 쓸 때 문제가 생긴다 ㅜ
  * JPA + H2 는 잘 되었던거 같은데... 이런걸 성숙도라고 하는 것 같다
  * (20/8/30 추가) Github Action으로 CI를 붙여보았는데 테스트에 사용할 MySQL 인스턴스 띄우는게 너무너무 쉽고 잘 동작해서 그냥 mysql 대상으로 인테그레이션 테스트 돌리는게 이제는 여러모로 좋다고 생각한다.
* fifthsage님의 [express+typeorm+jest+graphql 테스트환경 구축](https://fifthsage.github.io/%ED%85%8C%EC%8A%A4%ED%8A%B8/nodejs/express/jest/express+typeorm+jest+graphql-%ED%85%8C%EC%8A%A4%ED%8A%B8%ED%99%98%EA%B2%BD-%EA%B5%AC%EC%B6%95/) 라는 글도 도움이 많이 되었다.

### 결론
* 노드로 뭔가 빠르게 만들고 싶으면 이걸 쓸거 같다 (요새 사람들이 많이 얘기하는 nest.js에도 이게 붙어있다)
* 근데 그럴 일이 없는게 문제 -_-;;
* 작업한 내용은 https://github.com/zxzl/koa-apollo-typeorm 여기에 올려놓았다.
