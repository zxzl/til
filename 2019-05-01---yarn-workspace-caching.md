---
template: post
slug: /yarn-workspace-caching
title: "Yarn workspace 도커 이미지 캐싱"
author: "Hyeungshik Jung"
date: 2019-05-01 20:26:57
category: "til"
draft: false
tags:
- yarn
- monorepo
---

모노리포에 대해서는 [Rokt33r - Monorepo and Lerna](https://rokt33r.github.io/devnotes/2017/03/08/monorepo-and-lerna/)에서 설명을 잘 해주고 있고, 이 글은 [Yarn workspace](https://yarnpkg.com/lang/en/docs/workspaces/)를 사용하는 것을 가정. 

### 문제
아무래도 패키지가 많아지니까 이들이 의존하는 모듈을 설치하는데 시간이 좀 오래 걸리는데, 평소에 로컬이나 젠킨스 환경에서 빌드를 하면 이전에 설치해놓았던 모듈들이 그대로 남아있어서 큰 불편을 느끼지 못했다. 그러다가 이번에 도커에서 빌드를 하기로 하면서 어떻게 해야 효율적으로 모듈들이 설치된 레이어를 재사용 할 수 있을까 방법을 찾아보았다.


### 방법들

결론부터 말하면 깔아야 할 패키지가 같다면 패키지를 새로 설치하지 않는 것이다.

1. `**/node_modules`를 캐시로 사용하기

[Caching Yarn workspaces on CircleCI](https://www.benpickles.com/article법/77-caching-yarn-workspaces-on-circleci/?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+benpickles+%28Ben+Pickles%29)라는 블로그 포스팅에서 본 방법으로 최상단의 `node_modules` 폴더만 캐싱하지 말고 각 패키지 안에 들어있는 `node_modules`들도 함께 캐싱하는 것이다 (물론 yarn.lock이 바뀌면 캐시를 무효화). CircleCI 처럼 캐싱할 폴더를 지정할 수 있고, 또 workspace 갯수가 많지 않다면 가장 깔끔한 방법이라고 할 수 있겠다. 다만 도커에서 비슷한 느낌을 내려고 일단 패키지 전체를 설치하고, `**/node_modules`만 복사해서 쓰는 방법을 해보고 있는데 복사하는 과정에서 뭔가 링크가 깨지는지 잘 안 됬다. 아니면 아예 폴더를 옮기지 말고 소스만 지우는 방법도 있을 것 같은데 이건 해보고서 업데이트.

2. `**/package.json`이랑 `yarn.lock`만 복사해서 도커 레이어를 재사용하기

[Caching strategy for dependencies with Docker - lerna/lerna#858](https://github.com/lerna/lerna/issues/858#issuecomment-399699208)에서 본 것으로, 도커에서 파일이나 폴더를 복사할 때 체크섬이 같아야 레이어가 재활용이 된다는 점을 이용해서 전체 소스 코드 대신 `yarn.lock`이랑 `package.json`, 그리고 각 workspace의 `package.json`만 복사해서 모듈을 설치하는 것이다. 이러면 소스코드가 바뀌어도 저 파일들이 바뀌지 않으면 의존성이 설치된 상태까지의 도커까지는 재사용이 되니까 이미지 만드는 속도가 훨씬 빨리진다. ~~이것도 따라해 봤는데 저렇게만 복사해서는 아예 yarn 명령어로 설치가 안되었다~~.

(5/16) 오늘 2번을 다시 해봤는데 설치가 안되는건 package.json, tsconfig 등에 다른 모듈을 적어주지 않아서 그랬고 잘 적어주니까 깔끔하게 잘 되었다. 아래 써있는 방법에 비해서 관리도 편하고 속도도 빠르고 여려모로 만족.

아쉬운대로 일단 전체 설치한 이미지를 base로 삼아서 그 위에서 새로 코드를 받고, yarn을 돌려서 필요한 패키지가 있으면 새로 받는 것도 나름 방법이 되는데, 안 쓰는 소스코드가 남아있는 것이 뭔가 찜찜해서 좋은 방법은 아닌 것 같다. 1이나 2중에서 잘 되는 방법을 찾아서 이 포스트를 업데이트 할 수 있으면 좋겠다.
