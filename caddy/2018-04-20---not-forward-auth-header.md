---
title: Caddy를 이용해서 AWS Elasticsearch에 간단한 인증을 붙이기
slug: /not-forward-auth-header
date: "2018-04-20"
template: post
draft: false
category: "til"
tags:
    - caddy
---

AWS ES 에 인증을 붙이고 싶은데, IAM 같은 걸 쓰는건 복잡하고 아이디나 비밀번호를 써서 인증을 하고 싶을 때.

Caddy 로 그냥 proxy 를 붙이면 basicauth 때문에 생긴 Authorization Header 가 AWS ES 에 그대로 들어가서 에러가 난다.

```
kibana.hyeungshikjung.com {
     gzip
     basicauth / id pass
     proxy / https://search-lol-rcx7zrououmaytn3wngmyy7bwm.ap-northeast-2.es.amazonaws.com/_plugin/kibana/ {
       transparent
     }
 }
```

역시나 누군가 GitHub Issue[#1508](https://github.com/mholt/caddy/issues/1508)를 남기셔서 잘 따라했다. 이러면 Authorization 헤더는 딱 Caddy 에서 인증용으로만 사용되고 AWS ES 까지 전달되지 않는다.

```
kibana.hyeungshikjung.com {
     gzip
     basicauth / id pass
     proxy / https://search-lol-rcx7zrououmaytn3wngmyy7bwm.ap-northeast-2.es.amazonaws.com/_plugin/kibana/ {
         header_upstream -Authorization
     }
 }
```

오늘도 남들 덕분에 문제를 해결했다 -0-
