---
template: post
slug: /tsdb-for-prometheus-and-why-thanos
title: "프로메테우스의 스토리지 엔진 TSDB"
author: "Hyeungshik Jung"
date: 2021-01-31 00:08:49
category: "til"
draft: false
---


제목을 보시고 흥미가 가시면 아래 글을 읽으시면 되는데, 빠르게 겉만 핥고 싶은 분들이 계실까해서 공부 겸 정리해보았습니다.

* [Writing a Time Series Database from Scratch][1]
* [Introducing Thanos: Prometheus at scale][2]

---

## TSDB 

프로메테우스 백엔드로 별도 데이터베이스를 사용하는 경우도 많지만 기본적으로 로컬 디스크에 저장할 경우 tsdb 라는 스토리지엔진에 데이터를 저장한다 (결국은 디스크에 파일을 쓴다). 고가용성을 위해 Thanos 를 이용할 경우에도 가장 바닥에 있는 파일 포맷은 동일하므로 어떻게 생겼는지 알아둬서 나쁠 것은 없다.

[원래 포스팅][1]은 저장할 메트릭의 모양, 기존 구조의 문제점을 설명하느라고 앞부분이 긴데, 프로메테우스 현재 버전 (2점대)에서 사용하는 구조를 다룬 부분만 보면 은근히 내용이 간단하다.

- 시간 단위로 청크를 나눈다.
    - 2시간을 한 chunk의 단위로 한다면 현재~2시간 전 데이터는 메모리 + WAL에다가 쓰고, 2시간이 지난 데이터는 디스크에 저장한다고 한다.
    - mmap을 사용해서 어떤 chunk를 메모리에 놓을지, 언제 메모리에서 evict할지 등을 OS에 위임한다고 한다 (카프카가 페이지 캐시를 열심히 쓰는거랑 비슷하다 - 사실 둘 다 순서대로 append-only니까 비슷할 수 밖에 없다)
    - 리텐션 정책에 따라 더이상 안봐도 되는 데이터를 지워야 하는데, 그냥 더이상 필요없어진 chunk에 해당하는 디렉토리를 지우면 된다
- `compaction`: 존재하는 청크를 합치기도 한다
    - 청크의 최소 사이즈는 청크가 만들어지기 전 - 메모리에 저장하는 기간 - 을 적당히 짧게 유지하기 위해 정해진다
    - 하지만 쿼리하는 관점에서는 청크가 좀 더 큰 것이 좋다.
- 프로메테우스 사용하면 각 시리즈에 붙어있는 label을 많이 사용할텐데 라벨에 속하는 데이터를 찾기 위해서 inverted index를 사용한다.

여기서 더 깊게 들어가시고 싶다면 [Prometheus TSDB (Part 1): The Head Block](3) 시리즈에 정리가 되 있으니 가서 더 보셔도 좋겠다 (새로운 컨트리뷰터들이 구조를 빠르게 이해하고 작업에 들어갈 목적으로 적고 계신다고..)

## 로컬 디스크만 써도 저렇게 좋으면 타노스가 왜 필요할까?

사실 프로메테우스 서버 하나에서도 초당 백만개의 데이터를 집어넣을 수 있고, 말이 로컬 디스크지 EBS, Ceph을 마운트하면 내부적으로 복제를 하니까 꽤나 persistent하다. 그럼에도 불구하고 Thanos가 왜 필요한가...? 가 할 수 있다. [Thanos가 처음 나올 때 글][2]에 모티베이션이 잘 설명되어 있다 .

- 1: 프로메테우스 서버 여러개를 하나의 뷰로 보고 싶다

  - 프로메테우스 서버 별로 수집하는 메트릭이 다른데 그라파나에서 그래프 하나에 그리고 싶을 수 있다.
  - HA를 위해 같은 역할을 하는 프로메테우스 두 개를 띄운다고 할 때, 살아있는 프로메테우스에만 쿼리하기가 좀 귀찮을 수 있다.

- 2: 로컬디스크의 limit 이상의 데이터를 저장하고 싶다

- 3: 다운 샘플링이 필요하다 (5분/한시간 등등)

### 솔루션

1) 에 대해서, 각 프로메테우스 인스턴스 옆에다가 SideCar를 붙인 다음에 `Store API`를 만들었다. 요청이 하나 들어오면 `Querier`는 자기가 알고있는 프로메테우스들에다가 요청을 다 fan out한 다음에 합친다.

2) 에 대해서, SideCar는 `Querier`와 통신하는 것 외에 각 프로메테우스가 만든 chunk를 object storage에 백업해준다. 이걸 어떻게 쿼리에 써먹나..? `Store라` 컴포넌트는 프로메테우스 인스턴스의 SideCar처럼 `Store API` 를 구현하기 때문에 `Querier` 입장에서는 프로메테우스에 쿼리하나 Object storage에 쿼리하나 똑같다.

3) 프로메테우스는 compaction을 알아서 돌리는데 object storage에 있는 건 그렇지가 못하니 `Compactor` 라는 컴포넌트가 필요하다. Querier는 resolution에 따라서 알아서 compact된 데이터를 보기도 하고, 원래 데이터를 보기도 한다.

[1]:https://fabxc.org/tsdb/ 
[2]:https://www.improbable.io/blog/thanos-prometheus-at-scale
[3]: https://ganeshvernekar.com/blog/prometheus-tsdb-the-head-block
