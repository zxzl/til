---
template: post
slug: /zappa-s3-mistake
title: "Zappa로 배포한 애플리케이션에서 S3 관련 에러가(HeadBucket) 날 때"
author: "Hyeungshik Jung"
date: 2019-02-22 16:30:40
category: "til"
draft: false
tags:
- python
- zappa
- lambda
---

[Zappa](https://github.com/Miserlou/Zappa)로 Django나 Flask 앱을 배포할 때 흔히 static 파일은 s3에 올리게 되는데, zappa 배포도 잘 되고, django-stoarages 등을 이용한 static 파일 업로드도 잘 되었는데 배포된 URL에 들어가면 람다 함수에서 s3 버킷에 접근이 아예 안되는 경우가 있다. 이 때 여러가지 원인이 있겠지만 zappa_settings.json에서 
```json
   "profile_name": "your-profile-name", // AWS profile credentials to use. Default 'default'. Removing this setting will use the AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY environment variables instead.
```
이 `profile_name`을 의심해 볼 필요가 있다. 보통 프로젝트마다 AWS 프로필을 다르게 사용하려고 환경 변수에 있는 키를 사용하곤 하는데, 저 때 `profile_name`이 "default"로 설정되어 있으면 awscli에 등록된 키로 zappa가 돌아간다. 저 필드를 비우고 환경 변수에 있는 정보를 사용하면 static 파일을 올렸을 때와 람다 함수를 배포할 때 사용한 프로필이 같아져서 권한 문제가 발생할 확률이 크게 줄어든다.
