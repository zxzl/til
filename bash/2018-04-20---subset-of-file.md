---
title: 폴더 내에서 n번째 이후 파일만 선택하기
slug: /subset-of-file
date: "2016-04-20"
template: post
draft: false
category: "til"
tags:
    - "bash"
---
```
ls -d $PWD/* | awk 'NR > 5' | xargs -i echo {}
```

하면 이런식으로 나옴

```
/home/ubuntu/splitted/data_48m_05.json
/home/ubuntu/splitted/data_48m_06.json
/home/ubuntu/splitted/data_48m_07.json
/home/ubuntu/splitted/data_48m_08.json
/home/ubuntu/splitted/data_48m_09.json
/home/ubuntu/splitted/data_48m_10.json
/home/ubuntu/splitted/data_48m_11.json
/home/ubuntu/splitted/data_48m_12.json
/home/ubuntu/splitted/data_48m_13.json
/home/ubuntu/splitted/data_48m_14.json
/home/ubuntu/splitted/data_48m_15.json
/home/ubuntu/splitted/data_48m_16.json
/home/ubuntu/splitted/data_48m_17.json
/home/ubuntu/splitted/data_48m_18.json
/home/ubuntu/splitted/data_48m_19.json
/home/ubuntu/splitted/data_48m_20.json
/home/ubuntu/splitted/data_48m_21.json
/home/ubuntu/splitted/data_48m_22.json
/home/ubuntu/splitted/data_48m_23.json
/home/ubuntu/splitted/data_48m_24.json
/home/ubuntu/splitted/data_48m_25.json
/home/ubuntu/splitted/data_48m_26.json
/home/ubuntu/splitted/data_48m_27.json
/home/ubuntu/splitted/data_48m_28.json
/home/ubuntu/splitted/data_48m_29.json
/home/ubuntu/splitted/data_48m_30.json
/home/ubuntu/splitted/data_48m_31.json
/home/ubuntu/splitted/data_48m_32.json
/home/ubuntu/splitted/data_48m_33.json
/home/ubuntu/splitted/data_48m_34.json
/home/ubuntu/splitted/data_48m_35.json
/home/ubuntu/splitted/data_48m_36.json
/home/ubuntu/splitted/data_48m_37.json
/home/ubuntu/splitted/data_48m_38.json
/home/ubuntu/splitted/data_48m_39.json
/home/ubuntu/splitted/data_48m_40.json
/home/ubuntu/splitted/data_48m_41.json
/home/ubuntu/splitted/data_48m_42.json
/home/ubuntu/splitted/data_48m_43.json
/home/ubuntu/splitted/data_48m_44.json
/home/ubuntu/splitted/data_48m_45.json
/home/ubuntu/splitted/data_48m_46.json
/home/ubuntu/splitted/data_48m_47.json
```
